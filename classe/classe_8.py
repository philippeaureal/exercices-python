# cretation of the class
class Carre:
    """docstring forCarre."""
    def __init__(self, cote):
        self.cote = cote
        self.perimeter = self.perimeter()
        self.area = self.area()

# method that calculates the perimeter
    def perimeter(self):
        return self.cote * 4
# method that calculates the area
    def area(self):
        return self.cote * self.cote
# Override the representation method of the "Carre" class
    def __str__(self):
        """ methode d'affichage """
        return " le carré fait {} cm de coté, a un perimetre de {} cm et ça surface est de {} cm2".format(self.cote, self.perimeter, self.area )
# multiplication of the side of the square
    def factor(self, facteur):
        return Carre(self.cote * facteur)

# subtraction of two square
    def __sub__(self, autre):
        return Carre(self.cote - autre.cote)




# display condition

dimension = int(input("entrez la dimension du cote du carré "))
carre = Carre(dimension)
factor = int(input("tapez le facteur : "))

if __name__ == "__main__":



    print(carre)
    print(carre.factor(factor))



    objet2 = Carre(3).__sub__(Carre(2))
    print(objet2)

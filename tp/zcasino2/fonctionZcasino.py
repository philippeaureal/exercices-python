
def testNumeroMise():
    numeroMise = 51
    #int(input("sur quel numero de chance allez vous miser : "))
    while  numeroMise not in range(0, 50) :
        numeroMise = int(input("misez sur un nombre entre 0 et 49 compris :"))
    return numeroMise
def testMontantMise(montant, montantInitial):
    montant = int(input("combien voulez vous miser : "))
    while montant > montantInitial:
        montantInitial = 1000
        print("il vous reste : ", montantInitial)
        montant = int(input("vous n'avez pas assez, veuillez ressaisir un autre montant : "))
    return montant

def testCouleur(numMise, numGagnant):
    if numMise % 2 == 0:
        couleurMise = "noir"
    else:
        couleurMise = "rouge"

    if numGagnant % 2 == 0:
        couleurGagnant = "noir"
    else:
        couleurGagnant = "rouge"

import random
import math


continuer = "oui"
portefeuille = 1000
while continuer == "oui" :

    numeroMise = int(input("sur quel numero de chance allez vous miser : "))
    # test de la saisie du numéro misé
    while  numeroMise not in range(0, 50) :
        numeroMise = int(input("misez sur un nombre entre 0 et 49 compris :"))


    montantMise = int(input("combien voulez vous miser : "))

    #test du montant misé
    while montantMise > portefeuille:
        montantMise = int(input("vous n'avez pas assez, il vous reste : {} veuillez ressasir un autre montant : ".format(portefeuille)))

    # génerer le nombre aleatoire
    numeroGagnant = int(random.randrange(50))
    # test sur la couleur du nombre
    if numeroMise % 2 == 0:
        couleurNumeroMise = "noir"
    else:
        couleurNumeroMise = "rouge"

    if numeroGagnant % 2 == 0:
        couleurNumeroGagnant = "noir"
    else:
        couleurNumeroGagnant = "rouge"


    print("le numero misé est :  {} {}".format(numeroMise, couleurNumeroMise))
    print("le numéro gagnant est : {} {}".format(numeroGagnant, couleurNumeroGagnant ))

    # calcul du gain
    if numeroMise == numeroGagnant:
        montantGagne = montantMise * 3
        portefeuille = portefeuille + montantGagne
        print("JACKPOT!!!!! vous remportez : {}$ le montant de votre portefeuille est de : {}".format(montantGagne, portefeuille ))

    elif (numeroMise % 2) == 0 and (numeroGagnant % 2) == 0:
        montantGagne = math.ceil(montantMise / 2)
        portefeuille = portefeuille - montantGagne
        if portefeuille == 0:
            print("vous êtes ruiné, adieu")
        else:
            print(" vous n'avez pas gagné mais vous n'avez pas tout perdu, vous recupérez : {} $, il vous reste : {}".format(montantGagne, portefeuille))


    elif (numeroMise % 2) != 0 and (numeroGagnant % 2) != 0:
        montantGagne = math.ceil(montantMise / 2)
        portefeuille = portefeuille - montantGagne
        if portefeuille == 0:
            print("vous êtes ruiné, adieu")
        else:
            print(" vous n'avez pas gagné mais vous n'avez pas tout perdu, vous recupérez : {0} $, il vous reste : {1}".format(montantGagne, portefeuille))

    else:
        portefeuille = portefeuille - montantMise
        print("vous avez tout perdu, il vous reste : ", portefeuille)



if portefeuille == 0:
    print("vous êtes ruiné, adieu")
    continuer = "non"
else:
    continuer = input("voulez vous continuer? oui ou non : ")
            #calcul des gains

import pickle
import donnees


# verification de l'existence du fichier score et creation si besoin
# lire et ecrire le fichier "score"


def transformationChaine():
    chaineMot = list(motAtrouver)
    motCacher = []
    motCacher = ["*" for lettre in chaineMot]


def lectureScore(joueur):
    with open("score", "rb") as fichierJoueur:
        mesScore = pickle.Unpickler(fichierJoueur)
        score = mesScore.load()
        return score[joueur]

def ajoutJoueur(prenom):
    #joueur = input("votre nom :")
    newScore = {prenom : 0}
    with open("score", "rb") as score:
        newScore.update(pickle.load(score))

    with open("score", "wb") as score:
        pickle.dump(newScore, score)


def changementScore(joueur, point):


    with open("score", "rb") as score:
        newScore = pickle.load(score)

    newScore[joueur] = newScore[joueur] + point

    with open("score", "wb") as score:
        pickle.dump(newScore, score)

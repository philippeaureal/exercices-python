# create a list x of four tuples and diplay x
tuple1 = (7, 8)
tuple2 = (9, 10)
tuple3 = (11, 12)
tuple4 = (13, 14)
x = [tuple1, tuple2, tuple3, tuple4]
print(x)
#add a string "a" to end of the list x and display her
x.append("a")
print(x)
# add a string "b" to end of the list in different way and display her
x.insert(len(x), "b")
print(x)
# create list y with the values "1", "2", "3", add the items of the list y to the list xself.
# display x
y = ["1", "2", "3"]

x.extend(y)
print(x)
# add the value "2" in index four to list x, without remove existing index or replace it
#display x
x.insert(3, "2")
print(x)
# remove the first value "2" only on the list x
x.remove("2")
print(x)
# display y
print(y)
# create a list "z" and assign the same values of list y
# display list z
z = y
print(z)
# remove all items of list y and display it
y.clear()
print(y)
# remove all items of list z in different way then display it
z = []
print(z)

# create a dictionary
x = {"key":"valeur", "key2":"valeur2"}
# display value of "key"
print(x["key"])
# add value "toto" to key "titi"
x["titi"] = "toto"
# replace key "titi" to "tata" and display list x
x["tata"] = x["titi"]
print(x)
#delete key "titi" and his value
del x["titi"]
# delete key "key" and display his value
valueKey = x.pop("key")
print(valueKey)
#display list x
print(x)
# create a dictionary "y" and affect him the dictionary "x" and display him
y = x
print(y)

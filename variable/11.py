# déclarer le variable a et lui affecter la valeur 10
a = 10
# afficher a
print("a =", a)
# afficher le résultat de la division de a par 2
print("a divisé par 2 =", a / 2)
# afficher le resultat entier de la division de a par 2
print("le nombre entier de la division de a par 2 =",int(a // 2))
# afficher le reste de la division de a par 2
print("le reste de la division de a par 2 =", a % 2)
# afficher a puissance 3
print("a puissance 3 =",a ** 3)

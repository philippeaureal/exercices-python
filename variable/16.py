# declaration of lists
x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# display the value of the 4th element
print(x[3])
# display numbers between index 3 includes and excludes 5
print(x[3:5])
# display the numbers between index2 includes and index 8 excludes, by jump of 2
print(x[2:8:2])
# display the lenght of the list x
print(len(x))
# display the minimum value of x
x.sort()
print(x[0])
# display the maximum value of x
x.sort(reverse=True)
print(x[0])
# display the sum of the values of the list x
print(sum(x))
# remove numbers between 3 includes and 5 excludes and display list x

del x[3:5]
print(x)
